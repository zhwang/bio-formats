package loci.formats.in;

import loci.common.RandomAccessInputStream;
import loci.formats.*;
import loci.formats.meta.MetadataStore;
import ome.units.UNITS;
import ome.units.quantity.Length;
import ome.units.quantity.Time;

import java.io.IOException;

public class VisualizationGroupDatasetsReader extends FormatReader {
//    public VisualizationGroupDatasetsReader(String format, String suffix) {
//        super(format, suffix);
//    }
    // -- Constants --

    /** Specifies endianness. */
    private static final boolean IS_LITTLE = true;

    private static final int _bytesPerPixel = 2;

    private int getByteSizeX(){ return getSizeX()*_bytesPerPixel; }
    private int getByteSizeY(){ return getSizeY()*_bytesPerPixel; }
    private int getByteSizeZ(){ return getSizeZ()*_bytesPerPixel; }

    // -- Fields --

    /** Offsets to each image. */
    private int[] offsets;

    public VisualizationGroupDatasetsReader() {
        super("Visualization Group Datasets", new String[] {"dat"});
        domains = new String[] {FormatTools.HISTOLOGY_DOMAIN};
        suffixNecessary = true;
    }

    // -- IFormatReader API methods --

    /* @see loci.formats.IFormatReader#isThisType(RandomAccessInputStream) */
    @Override
    public boolean isThisType(RandomAccessInputStream stream) throws IOException {
        final int blockLen = 4;
        if (!FormatTools.validStream(stream, blockLen, IS_LITTLE)) {
            return false;
        }


        return true;
    }

    /**
     * @see loci.formats.IFormatReader#openBytes(int, byte[], int, int, int, int)
     */
    @Override
    public byte[] openBytes(int no, byte[] buf, int x, int y, int w, int h)
            throws FormatException, IOException
    {
        FormatTools.checkPlaneParameters(this, no, buf.length, x, y, w, h);

        in.seek(offsets[no] + getByteSizeX() * (getSizeY() - y - h)*_bytesPerPixel);

        for (int row=h-1; row>=0; row--) {
            in.skipBytes(x*_bytesPerPixel);
            in.read(buf, row*w*_bytesPerPixel, w*_bytesPerPixel);
            in.skipBytes((getSizeX() - w - x)*_bytesPerPixel);
        }
        return buf;
    }

    /* @see loci.formats.IFormatReader#close(boolean) */
    @Override
    public void close(boolean fileOnly) throws IOException {
        super.close(fileOnly);
        if (!fileOnly) offsets = null;
    }

    // -- Internal FormatReader API methods --

    /* @see loci.formats.FormatReader#initFile(String) */
    @Override
    protected void initFile(String id) throws FormatException, IOException {
        super.initFile(id);
        in = new RandomAccessInputStream(id);

        LOGGER.info("Verifying Visualization Group Data sets");

        in.order(IS_LITTLE);

        LOGGER.info("Reading header");

        CoreMetadata m = core.get(0);

        m.sizeX = in.readShort();
        m.sizeY = in.readShort();
        m.sizeZ = in.readShort();
        m.sizeC = 1;

        LOGGER.info("Calculating image offsets");

        m.imageCount = getSizeZ() * getSizeC();
        offsets = new int[getImageCount()];
        int offset = 6;
        for (int i=0; i<getSizeC(); i++) {
            for (int j=0; j<getSizeZ(); j++) {
                offsets[i*getSizeZ() + j] = offset + (j * getByteSizeX() * getByteSizeY());
            }
            offset += getByteSizeX() * getByteSizeY() * getByteSizeZ();
        }


        LOGGER.info("Populating metadata");

        m.sizeT = getImageCount() / (getSizeC() * getSizeZ());
        m.dimensionOrder = "XYZCT";
        m.rgb = false;
        m.interleaved = false;
        m.littleEndian = IS_LITTLE;
        m.indexed = false;
        m.falseColor = false;
        m.metadataComplete = true;
        m.pixelType = FormatTools.UINT16;

        // The metadata store we're working with.
        MetadataStore store = makeFilterMetadata();
        MetadataTools.populatePixels(store, this);

        // populate Image data
        String imageName = "VGD Image";
        store.setImageName(imageName, 0);

        if (getMetadataOptions().getMetadataLevel() != MetadataLevel.MINIMUM) {
            store.setImageDescription("VGD description", 0);

            // link Instrument and Image
            String instrumentID = MetadataTools.createLSID("Instrument", 0);
            store.setInstrumentID(instrumentID, 0);
            store.setImageInstrumentRef(instrumentID, 0);

            // populate Dimensions data

            Length sizeX = FormatTools.getPhysicalSizeX((double) 1.0);
            Length sizeY = FormatTools.getPhysicalSizeY((double) 1.0);
            Length sizeZ = FormatTools.getPhysicalSizeZ((double) 1.0);

            if (sizeX != null) {
                store.setPixelsPhysicalSizeX(sizeX, 0);
            }
            if (sizeY != null) {
                store.setPixelsPhysicalSizeY(sizeY, 0);
            }
            if (sizeZ != null) {
                store.setPixelsPhysicalSizeZ(sizeZ, 0);
            }
            store.setPixelsTimeIncrement(new Time(1.0, UNITS.SECOND), 0);

            // populate Detector data
            for (int i=0; i<getSizeC(); i++) {
                store.setDetectorSettingsOffset((double) offsets[i], i, 0);
                // link DetectorSettings to an actual Detector
                String detectorID = MetadataTools.createLSID("Detector", 0, i);
                store.setDetectorID(detectorID, 0, i);
                store.setDetectorType(MetadataTools.getDetectorType("Other"), 0, i);
                store.setDetectorSettingsID(detectorID, 0, i);
            }
        }
    }
}
